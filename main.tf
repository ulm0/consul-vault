provider "helm" {}

resource "helm_release" "consul" {
  name       = "consul"
  chart      = "consul"
  repository = "https://helm.releases.hashicorp.com"
  version    = "v0.21.0"
  namespace  = "kube-system"
  atomic     = true
  values     = local.consul_values
}

data "kubernetes_secret" "consul_token" {
  depends_on = [helm_release.consul]
  metadata {
    name      = format("%s-consul-bootstrap-acl-token", helm_release.consul.metadata.0.name)
    namespace = helm_release.consul.metadata.0.namespace
  }
}

resource "helm_release" "vault" {
  depends_on    = [helm_release.consul]
  name          = "vault"
  chart         = "vault"
  repository    = "http://kubernetes-charts.banzaicloud.com/branch/master"
  version       = "1.3.0"
  namespace     = helm_release.consul.metadata.0.namespace
  atomic        = true
  recreate_pods = true
  values        = local.vault_values
}
