.PHONY: init
init:
	@terraform init

.PHONY: plan
plan: init
	@terraform plan

.PHONY: apply
apply: plan
	@terraform apply -auto-approve

.PHONY: destroy
destroy: init
	@terraform destroy -auto-approve
