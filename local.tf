locals {
  consul_values = [
    yamlencode(
      {
        global = {
          enabled = true
          acls = {
            manageSystemACLs = true
          }
          tls = {
            enabled = false
          }
        }

        server = {
          replicas        = 3
          bootstrapExpect = 3
          affinity        = {}
          storage         = "2Gi"
          # resources = {
          #   requests = {
          #     memory = "1024Mi"
          #     cpu    = "250m"
          #   }

          #   limits = {
          #     memory = "4096Mi"
          #     cpu    = "500m"
          #   }
          # }
        }

        client = {
          enabled = false
        }

        ui = {
          enabled = true
          service = {
            type = "NodePort"
          }
        }
      }
    )
  ]

  vault_values = [
    yamlencode(
      {
        replicaCount = 2

        service = {
          type = "NodePort"
        }

        unsealer = {
          # Make args namespace-aware
          # https://github.com/banzaicloud/bank-vaults/blob/chart/vault/1.3.0/charts/vault/values.yaml#L197
          args = [
            "--mode",
            "k8s",
            "--k8s-secret-namespace",
            format("%s", helm_release.consul.metadata.0.namespace),
            "--k8s-secret-name",
            "bank-vaults",
          ]
        }

        vault = {
          config = {
            listener = {
              tcp = {
                tls_disable = true
              }
            }

            storage = {
              consul = {
                address = format("%s-consul-server.%s.svc:8500", helm_release.consul.metadata.0.name, helm_release.consul.metadata.0.namespace)
                path    = "vault"
                token   = data.kubernetes_secret.consul_token.data["token"]
              }
            }
          }
        }
      }
    )
  ]
}
